#
# Cookbook Name:: docker-for-oel70
# Recipe:: wls-admin-container
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "start-admin-container" do
  user "root"
  code <<-EOS
    docker run -d --name wlsadmin -p 8001:8001 samplewls:12.1.3
  EOS
end
