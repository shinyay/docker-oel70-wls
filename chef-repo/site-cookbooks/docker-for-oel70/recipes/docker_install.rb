#
# Cookbook Name:: docker-for-oel70
# Recipe:: docker-install
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "docker" do
  action :install
end

service "docker" do
  supports :start => true, :status => true, :restart => true, :reload => true
  action [:enable, :start]
end
