#
# Cookbook Name:: docker-for-oel70
# Recipe:: docker_build
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

docker_wls = "#{node[:docker][:docker_repo]}/OracleWebLogic"

bash "docker-image-build" do
  user "root"
  code <<-EOS
    cd #{docker_wls}/dockerfiles
    ./buildDockerImage.sh -g    
  EOS
end

bash "sleep" do
  action :nothing
  command "sleep 5"
end

bash "docker-build" do
  user "root"
  code <<-EOS
    docker build -t samplewls:12.1.3 #{docker_wls}/samples/12c-domain
  EOS
end
