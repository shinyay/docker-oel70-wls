#
# Cookbook Name:: docker-for-oel70
# Recipe:: wls-image
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

image_home = "#{node[:docker][:install_image_dir]}"
docker_install_image = "#{node[:docker][:docker_repo]}/OracleWebLogic/dockerfiles/12.1.3"

file "#{docker_install_image}/jdk-7u79-linux-x64.rpm" do
  content IO.read( "#{image_home}/jdk/jdk-7u79-linux-x64.rpm" )
end

file "#{docker_install_image}/fmw_12.1.3.0.0_wls.jar" do
  content IO.read( "#{image_home}/wls12c/fmw_12.1.3.0.0_wls.jar" )
end
