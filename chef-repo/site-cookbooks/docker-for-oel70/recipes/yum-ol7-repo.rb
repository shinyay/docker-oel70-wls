#
# Cookbook Name:: docker-for-oel70
# Recipe:: yum-ol7-repo.rb
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

repo_home = "/etc/yum.repos.d"

file "#{repo_home}/public-yum-ol7.repo.org" do
  content IO.read( "#{repo_home}/public-yum-ol7.repo" )
end

bash "enable-ol7-addons" do
  user "root"
  code <<-EOS
    sed -i -e "$(grep -n "ol7_addons" #{repo_home}/public-yum-ol7.repo | cut -d: -f1),$(grep -n "ol7_addons" #{repo_home}/public-yum-ol7.repo | cut -d: -f1 |awk '{print $1+5}')s/enabled=0/enabled=1/" #{repo_home}/public-yum-ol7.repo
  EOS
end
