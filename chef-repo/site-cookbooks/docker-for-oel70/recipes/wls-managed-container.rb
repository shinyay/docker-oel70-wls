#
# Cookbook Name:: docker-for-oel70
# Recipe:: wls-managed-container
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "start-managed-container" do
  user "root"
  code <<-EOS
    docker run -d --link wlsadmin:wlsadmin --name="managed1" -p 7002:7002 -e MS_PORT="7002" samplewls:12.1.3 createServer.sh
  EOS
end
